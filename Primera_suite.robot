*** Settings ***
Documentation     Existe en un documento de texto los pasos manuales
Resource          ./recursos.resource

*** Variables ***

*** Test Cases ***
G001 Búsqueda de palabras en google
    Abrir navegador y esperar log
    Input Text    xpath=//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input    ${palabraABuscar}
    Click Element    xpath=//*[@id="hplogo"]
    Click Element    xpath=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]
    Title Should Be    ${palabraABuscar} - Buscar con Google
    Page Should Contain    ${palabraABuscar}
    Close Browser

G002 Hacer click en el botón de búsqueda sin escribir palabras en Google
    Abrir navegador y esperar log
    Click Element    xpath=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]
    Title Should Be    Google
    Close Browser

*** Keywords ***
